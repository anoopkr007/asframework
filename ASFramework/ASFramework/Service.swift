//
//  Service.swift
//  ASFramework
//
//  Created by Daffolapmac-146 on 06/09/21.
//

import Foundation

public class Service {
    
    private init () {}
    
    public static func doSomething() -> String {
        return "This is an example of private pod and install using cocoapods"
    }
}
